package com.example.kafka.schedulers;

import com.example.kafka.service.ProducerService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class KafkaScheduler {


    ProducerService producerService;

    public KafkaScheduler(ProducerService producerService) {
        this.producerService = producerService;
    }

    @Scheduled(fixedDelay = 10000, initialDelay = 3000)
    public void kafkaScheduler() {
        Random rand = new Random();
        int value = rand.nextInt();
        producerService.sendFromProducer(String.valueOf(value), "random");
    }
}
