package com.example.kafka.service;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
public class ProducerService {


    KafkaTemplate kafkaTemplate;

    public ProducerService(KafkaTemplate kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendFromProducer(final String message, final String topic) {
        ListenableFuture<SendResult<String, Object>> fut = kafkaTemplate.send(topic, message);
        fut.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
            }
        });
    }
}
