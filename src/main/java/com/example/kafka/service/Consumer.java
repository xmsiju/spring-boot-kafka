package com.example.kafka.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @KafkaListener(topics = "random", groupId = "group1")
    public void listenMessage(final String message) {
        System.out.println("message from Listener=" + message);
    }
}
